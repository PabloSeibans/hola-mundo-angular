import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  /* Aca funciona porque esta fuera del constructor pero igual se puede poner ahi*/
  //anioActual = new Date();

  anio;
  //anio = this.anioActual.getFullYear();

  constructor() {
    this.anio = new Date().getFullYear();
  }

  ngOnInit(): void {
  }

}
